%include "words.inc"
%include "lib.inc"

global _start
%define KEY_LEN 256

section .data
input_message: db 'Enter the key: ', 0
success_message: db 'I found the key: ', 0 
fail_message: db 'I cant find the key.', 0xA, 0
key: times 256 db 0
section .text

read_string:
    mov r8, rdi
    mov r9, rsi 
    mov r10, 0 
.next_char:
    call read_char
    cmp rax, 0xA
    je .success
    inc r10
    cmp r10, r9
    je .error
    mov [r8+r10-1], al
    jmp .next_char
.success:
    mov byte [r8+r10], 0x0
    mov rax, r8
    mov rdx, r10
    xor rax, rax
    ret
.error:
    mov rax, 1
    ret


print_error:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    syscall
    xor rax, rax
    ret

_start:
    sub rsp, KEY_LEN
    mov rdi, rsp
    mov rdi, input_message
    call print_string
    mov rdi, key
    mov rsi, 256
    call read_string
    jnz .fail
    mov rdi, key
    mov rsi, ptr
    call find_word
    test rax, rax
    jz .fail
    mov rdi, rax
    add rdi, 8
.skip:
    inc rdi
    cmp byte [rdi], 0
    jne .skip
    inc rdi
    call print_string
    call print_newline
    xor rax, rax
    call exit
.fail:
    mov rdi, fail_message
    call print_error
    mov rax, -1
    call exit
