ASM=nasm
LD=ld
ASM_FLAGS=-f elf64

main: main.o lib.o dict.o
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<