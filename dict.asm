global find_word
extern string_equals
section .text

find_word:
    test rsi, rsi ; проверяем, существует ли следующее слово
    jz .fail ; если нет, то все плохо
    push rdi ; сохраняем адрес искомой строки
    push rsi ; сохраняем адрес ключа в словаре
    add rsi, 8 ; прибавляем к адресу словаря 8 байт
    call string_equals ; проверяем, совпадают ли строки
    pop rsi ; получаем обратно наш адрес
    pop rdi ; и получаем обратно адрес словаря
    test rax, rax ; если не равон нулю
    jnz .success ; то значит строки совпадают и возвращаем 
    mov rsi, [rsi] ; иначе двигаем указатель на текущее слово
    jmp find_word  ; и повторяем все заново
.success:
    mov rax, rsi ; возвращает адрес начало вхождения слова в словарь
    ret
.fail:
    xor rax, rax ; возвращает 0
    ret
